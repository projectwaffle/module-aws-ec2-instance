output "id" {
  description = "The ID of the Instance"
  value       = "${aws_instance.main.*.id}"
}

output "password_data" {
  description = "The Password Data of the Instance"
  value       = "${aws_instance.main.*.password_data}"
}

output "public_dns" {
  description = "The Public DNS of the Instance"
  value       = "${aws_instance.main.*.public_dns}"
}

output "public_ip" {
  description = "The Public IP of the Instance"
  value       = "${aws_instance.main.*.public_ip}"
}

output "private_dns" {
  description = "The Private DNS of the Instance"
  value       = "${aws_instance.main.*.private_dns}"
}

output "private_ip" {
  description = "The Private IP of the Instance"
  value       = "${aws_instance.main.*.private_ip}"
}

output "eip_public_ip" {
  description = "The EIP Addresses if assigned to interface"
  value       = "${concat(aws_eip.main.*.public_ip, list(""))}"
}
