## Global variables

variable "resource_identities" {
  description = "Resource Identity according to PW Arhitecture Name Convention"
  type        = "map"
}

variable "global_tags" {
  description = "Global Tags"
  type        = "map"
}

## Module Specific variables

variable "module_tags" {
  description = "Module Tags"
  default     = {}
}

variable "name" {
  description = "Name for all resources to start with"
}

## Resource variables

# Key Pair

variable "key_name" {
  description = "Existing AWS SSH key Name used to connect to Instance"
  default     = ""
}

variable "public_key" {
  description = "The public key material"
  default     = ""
}

variable "create_key_pair" {
  description = "Create Key Pair by providing public key of own private one"
  default     = "false"
}

# EC2 Insatance

variable "create_instance" {
  description = "Set to false if you do not want to create EC2 Instance for module"
  default     = "false"
}

variable "instance_os" {
  description = "Instance OS Type. Can be linux or windows"
  default     = "linux"
}

variable "instance_count" {
  description = "Number of Instances OS Type"
  default     = "1"
}

variable "enable_encryption" {
  description = "Enables EBS Encryption Defaults to false. Setting this to true will create KMS key and encrypt EBS with it"
  default     = "false"
}

variable "instance_type" {
  description = "Instance Type. Select instance type from approved AWS list in the region"
  default     = "t3.nano"
}

variable "instance_name_prefix" {
  description = "Instance Name Prefix for Tagging the Name. It will add count inxed at the end plus 1"
  default     = ""
}

variable "ebs_optimized" {
  description = "If true, the launched EC2 instance will be EBS-optimized. Note that if this is not set on an instance type that is optimized by default then this will show as disabled but if the instance type is optimized by default then there is no need to set this and there is no effect to disabling it"
  default     = "false"
}

variable "disable_api_termination" {
  description = "If true, enables EC2 Instance Termination Protection"
  default     = "false"
}

variable "cpu_credits" {
  description = "The credit option for CPU usage. Can be standard or unlimited"
  default     = "standard"
}

variable "vpc_security_group_ids" {
  description = "List of security groups"
  type        = "list"
}

variable "subnet_id" {
  description = "List of subnet IDs"
  type        = "list"
}

variable "subnet_az" {
  description = "List of subnet AZs"
  type        = "list"
}

variable "create_custom_userdata" {
  description = "Create custome Userdata script"
  default     = "false"
}

variable "custom_userdata_script_path" {
  description = "Custome Userdata script path - optional - only one works `var.custom_userdata_script_path` or `var.custom_userdata_script_content`"
  default     = ""
}

variable "custom_userdata_script_content" {
  description = "Custome Userdata script content - optional - only one works `var.custom_userdata_script_path` or `var.custom_userdata_script_content`"
  default     = ""
}

variable "image_id" {
  description = "Static imgae ID for input"
  default     = ""
}

variable "instance_tags" {
  description = "Custome EC2 Instance Tags"
  default     = {}
}

# Root EBS Volume

variable "root_block_device_ebs_delete_on_termination" {
  description = "Whether the volume should be destroyed on instance termination"
  default     = "true"
}

variable "root_block_device_ebs_volume_size_defaults" {
  description = "Defaults for the size of the root volume in gigabytes"

  default = {
    linux   = "8"
    windows = "32"
  }
}

variable "root_block_device_ebs_volume_size" {
  description = "The size of the root volume in gigabytes"
  default     = ""
}

variable "root_block_device_ebs_volume_type" {
  description = "The type of root volume. Can be standard, gp2, or io1"
  default     = "gp2"
}

variable "root_block_device_ebs_iops" {
  description = "The amount of provisioned IOPS for root device. This must be set with a volume_type of io1"
  default     = "250"
}

variable "root_block_device_tags" {
  description = "Custome EC2 Instance Root Volume tags Tags"
  default     = {}
}

# Data EBS Volume

variable "create_data_block_device_ebs" {
  description = "Set to false if you do not want to create Data disk for EC2 Instance for module"
  default     = "false"
}

variable "data_block_device_ebs_count" {
  description = "Data disk count to create for the instance"
  default     = "1"
}

variable "data_block_device_ebs_volume_size" {
  description = "The size of the data volume in gigabytes"
  default     = "8"
}

variable "data_block_device_ebs_volume_type" {
  description = "The type of data volume. Can be standard, gp2, or io1"
  default     = "gp2"
}

variable "data_block_device_ebs_iops" {
  description = "The amount of provisioned IOPS for data device. This must be set with a volume_type of io1"
  default     = "250"
}

variable "data_block_device_ebs_snapshot_id" {
  description = "The Snapshot ID to mount data device block"
  default     = ""
}

variable "force_detach" {
  description = "Set to true if you want to force the volume to detach. Useful if previous attempts failed, but use this option only as a last resort, as this can result in data loss."
  default     = "true"
}

variable "skip_destroy" {
  description = "Set this to true if you do not wish to detach the volume from the instance to which it is attached at destroy time, and instead just remove the attachment from Terraform state."
  default     = "false"
}

variable "iam_instance_profile" {
  description = " The IAM Instance Profile to launch the instance with. Specified as the name of the Instance Profile"
  default     = ""
}

variable "data_block_device_tags" {
  description = "Custome EC2 Instance Root Volume tags Tags"
  default     = {}
}

# Elastic IP

variable "create_eip" {
  description = "Set to false if you do not want to create EIP EC2 Instance for module"
  default     = "false"
}

variable "eip_tags" {
  description = "Custome Elastic IP Tags"
  default     = {}
}

# KMS Key Encryption

variable "kms_key_id" {
  description = "KMS Key ID used for encryption of the logs. works with `var.enable_encryption`"
  default     = ""
}

variable "target_group_arns" {
  description = "A list of `aws_alb_target_group` ARNs, for use with Application Load Balancing."
  default     = []
}

variable "associate_target_group_arns" {
  description = "Enable Target Group association with the list from `var.target_group_arns`"
  default     = "false"
}

variable "associate_target_group_instance_ip" {
  description = "Enable if association target group with instances private IP address"
  default     = "false"
}
