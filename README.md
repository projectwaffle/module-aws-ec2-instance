# AWS EC instance Terraform module

[![CircleCI](https://circleci.com/bb/projectwaffle/module-aws-ec2-instance.svg?style=svg)](https://circleci.com/bb/projectwaffle/module-aws-ec2-instance/)

Terraform module which creates EC2 Instace and related resources on AWS. Might be used for different modules

These types of resources are supported:

* [EC2 Instace](https://www.terraform.io/docs/providers/aws/r/instance.html)
* [AutoScaling Group](https://www.terraform.io/docs/providers/aws/r/autoscaling_group.html)
* [Launch Temaplate](https://www.terraform.io/docs/providers/aws/r/launch_template.html)
* [EBS Volume](https://www.terraform.io/docs/providers/aws/r/ebs_volume.html)
* [Elastic IP](https://www.terraform.io/docs/providers/aws/r/eip.html)

Sponsored by [Draw.io - the best way to draw AWS diagrams](https://www.draw.io/)

## Usage

```hcl
module "instance" {
  source = "bitbucket.org/projectwaffle/module-aws-ec2-instance.git?ref=tags/v0.0.1"

  name = "training-prod"

  resource_identities = {
    key_name = "keypair"
    eip      = "eip"
  }

  global_tags = "${var.global_tags}"

  create_instance              = "true"
  vpc_security_group_ids       = ["${module.sg_web.security_group_id}"]
  subnet_id                    = "${module.vpc.public_subnets}"
  subnet_az                    = "${module.vpc.public_subnets_azs}"
  instance_name_prefix         = "app-p"
  key_name                     = "training-keypair-ireland"
  iam_instance_profile         = "${module.ec2_assume_role.assume_role_instance_profile_name}"
  create_data_block_device_ebs = "true"
  data_block_device_ebs_count  = "1"
}
```

Security group, subnet ID and Subnet AZ are imported from different modules. Instance profile as well. Can be specified just IDs

## Custom Userdata and Instance hostname

By default, module will create userdata to setup next default settings:

 * Set up hostname - useing variable `instance_name_prefix = "app-p"`
 * Do update to all software (amazon linux only)

There is an option to add custom userdata script with next:
```hcl
module "instance" {

  create_custom_userdata      = "true"
  custom_userdata_script_path = "./custom_script.sh"
}
```

Just point to custome script file.

## Resource Identity

In order to add custom names to resources, each module has two variables to be used for this.
* Variable called `name` - this will be used as the base name for each resource in the module
    * `name = "training-prod"`
* MAP variable called `resource_identities` - used to append specific name to each resoruce
```hcl
  resource_identities = {
    config_config_rule            = "config-rule"
    config_configuration_recorder = "config-recorder"
  }
```

Both variables should be declared in module section:
```hcl
module "config" {
  ......
  name = "training-prod"
  resource_identities = {
    instance            = "app"
  }
  ......
}
```

Next is an example of variable used in the Module resoruce names or tgas 'Name':
```hcl
  name  = "${format("%s-%s", var.name, var.resource_identities["instance"])}"
  tags  = "${merge(map("Name", format("%s-%s", var.name, var.resource_identities["instance"],)), var.global_tags, var.module_tags, var.instance_tags)}"
```

## Resource Tagging

There are three level of resource tagging declaration

* Global level - declare tagging strategy that will be applied on all the AWS resources

```hcl
variable "global_tags" {
  type = "map"

  default = {
    environment     = "prod"
    role            = "infrastructure"
    version         = "0.1"
    owner           = "Sergiu Plotnicu"
    bu              = "IT"
    customer        = "project waffle"
    project         = "training"
    confidentiality = "open"
    compliance      = "N/A"
    encryption      = "disabled"
  }
}
```

* Module level - taggig that will affect only resources created by the module itself
```hcl
module "instance" {

  module_tags = {
    bu = "security"
  }
  global_tags         = "${var.global_tags}"
}
```

* Resource level - each resource can be tagged with custome tag or tage that will rewrite all the others one

```hcl
variable "instance_tags" {
  description = "Custome Instance Tags"
  default     = {
    role = "app"
  }
}
```

Combination of all this variables should cover all the tagging requirements.
Tags are beeing merged so, resource level ones, will rewrite modules one, that will rewrite global ones.

## Key Pair management

EC2 instances need keypair to connect to SSH (linux) or generate Administrator password (Window system).
There are couple of ways to manage Keypair

* Variable called `key_name` - to use existing AWS Console or previously created Keypair
    * `key_name = "key_name = "training-keypair-ireland""`

* Create new Keypair. `public_key` required if `create_key_pair` is `true`

```hcl
module "instance" {

  create_key_pair = "true"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD3F6tyPEFEzV0LX3X8BsXdMsQz1x2cEikKDEY0aIj41qgxMCP/iteneqXSIFZBp5vizPvaoIR3Um9xK7PGoW8giupGn+EPuxIA4cDM4vzOqOkiMPhz5XK0whEjkVzTo4+S0puvDZuwIsdiW9mxhJc7tgBNL0cYlWSYVkz4G/fslNfRPW5mYAM49f4fhtxPb5ok4Q2Lg9dPKVHO/Bgeu5woMc7RY0p1ej6D4CKFE6lymSDJpW0YHX/wqE9+cfEauh7xZcG0q9t2ta6F6fmX0agvpFyZo8aFbXeUBr7osSCJNgvavWbM/06niWrOvYX2xwWdhXmXSrbX8ZbabVohBK41 sergiu.plotnicu@project-waffle.com"
}  
```  

* No Keypair at all - default Option. This option is useful when Instances are used in a imutable way either use of SSM agent to initiate session on instance. Details can be found here - [AWS Systems Manager Session Manager](https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager.html)

## Release

### Version v0.0.2

- Support for Target Group attachemnt with IP address

### Version v0.0.1

- Initial Version

## Terraform version

Terraform version 0.11.10 or newer is required for this module to work.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| associate\_target\_group\_arns | Enable Target Group association with the list from `var.target_group_arns` | string | `false` | no |
| associate\_target\_group\_instance\_ip | Enable if association target group with instances private IP address | string | `false` | no |
| cpu\_credits | The credit option for CPU usage. Can be standard or unlimited | string | `standard` | no |
| create\_custom\_userdata | Create custome Userdata script | string | `false` | no |
| create\_data\_block\_device\_ebs | Set to false if you do not want to create Data disk for EC2 Instance for module | string | `false` | no |
| create\_eip | Set to false if you do not want to create EIP EC2 Instance for module | string | `false` | no |
| create\_instance | Set to false if you do not want to create EC2 Instance for module | string | `false` | no |
| create\_key\_pair | Create Key Pair by providing public key of own private one | string | `false` | no |
| custom\_userdata\_script\_content | Custome Userdata script content - optional - only one works `var.custom_userdata_script_path` or `var.custom_userdata_script_content` | string | `` | no |
| custom\_userdata\_script\_path | Custome Userdata script path - optional - only one works `var.custom_userdata_script_path` or `var.custom_userdata_script_content` | string | `` | no |
| data\_block\_device\_ebs\_count | Data disk count to create for the instance | string | `1` | no |
| data\_block\_device\_ebs\_iops | The amount of provisioned IOPS for data device. This must be set with a volume_type of io1 | string | `250` | no |
| data\_block\_device\_ebs\_snapshot\_id | The Snapshot ID to mount data device block | string | `` | no |
| data\_block\_device\_ebs\_volume\_size | The size of the data volume in gigabytes | string | `8` | no |
| data\_block\_device\_ebs\_volume\_type | The type of data volume. Can be standard, gp2, or io1 | string | `gp2` | no |
| data\_block\_device\_tags | Custome EC2 Instance Root Volume tags Tags | map | `<map>` | no |
| disable\_api\_termination | If true, enables EC2 Instance Termination Protection | string | `false` | no |
| ebs\_optimized | If true, the launched EC2 instance will be EBS-optimized. Note that if this is not set on an instance type that is optimized by default then this will show as disabled but if the instance type is optimized by default then there is no need to set this and there is no effect to disabling it | string | `false` | no |
| eip\_tags | Custome Elastic IP Tags | map | `<map>` | no |
| enable\_encryption | Enables EBS Encryption Defaults to false. Setting this to true will create KMS key and encrypt EBS with it | string | `false` | no |
| force\_detach | Set to true if you want to force the volume to detach. Useful if previous attempts failed, but use this option only as a last resort, as this can result in data loss. | string | `true` | no |
| global\_tags | Global Tags | map | - | yes |
| iam\_instance\_profile | The IAM Instance Profile to launch the instance with. Specified as the name of the Instance Profile | string | `` | no |
| image\_id | Static imgae ID for input | string | `` | no |
| instance\_count | Number of Instances OS Type | string | `1` | no |
| instance\_name\_prefix | Instance Name Prefix for Tagging the Name. It will add count inxed at the end plus 1 | string | `` | no |
| instance\_os | Instance OS Type. Can be linux or windows | string | `linux` | no |
| instance\_tags | Custome EC2 Instance Tags | map | `<map>` | no |
| instance\_type | Instance Type. Select instance type from approved AWS list in the region | string | `t3.nano` | no |
| key\_name | Existing AWS SSH key Name used to connect to Instance | string | `` | no |
| kms\_key\_id | KMS Key ID used for encryption of the logs. works with `var.enable_encryption` | string | `` | no |
| module\_tags | Module Tags | map | `<map>` | no |
| name | Name for all resources to start with | string | - | yes |
| public\_key | The public key material | string | `` | no |
| resource\_identities | Resource Identity according to PW Arhitecture Name Convention | map | - | yes |
| root\_block\_device\_ebs\_delete\_on\_termination | Whether the volume should be destroyed on instance termination | string | `true` | no |
| root\_block\_device\_ebs\_iops | The amount of provisioned IOPS for root device. This must be set with a volume_type of io1 | string | `250` | no |
| root\_block\_device\_ebs\_volume\_size | The size of the root volume in gigabytes | string | `` | no |
| root\_block\_device\_ebs\_volume\_size\_defaults | Defaults for the size of the root volume in gigabytes | map | `<map>` | no |
| root\_block\_device\_ebs\_volume\_type | The type of root volume. Can be standard, gp2, or io1 | string | `gp2` | no |
| root\_block\_device\_tags | Custome EC2 Instance Root Volume tags Tags | map | `<map>` | no |
| skip\_destroy | Set this to true if you do not wish to detach the volume from the instance to which it is attached at destroy time, and instead just remove the attachment from Terraform state. | string | `false` | no |
| subnet\_az | List of subnet AZs | list | - | yes |
| subnet\_id | List of subnet IDs | list | - | yes |
| target\_group\_arns | A list of `aws_alb_target_group` ARNs, for use with Application Load Balancing. | list | `<list>` | no |
| vpc\_security\_group\_ids | List of security groups | list | - | yes |

## Outputs

| Name | Description |
|------|-------------|
| eip\_public\_ip | The EIP Addresses if assigned to interface |
| id | The ID of the Instance |
| password\_data | The Password Data of the Instance |
| private\_dns | The Private DNS of the Instance |
| private\_ip | The Private IP of the Instance |
| public\_dns | The Public DNS of the Instance |
| public\_ip | The Public IP of the Instance |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->


## Authors

Module is maintained by [Sergiu Plotnicu](https://bitbucket.org/projectwaffle/)

## License

Licensed under Sergiu Plotnicu, please contact him at - sergiu.plotnicu@gmail.com


