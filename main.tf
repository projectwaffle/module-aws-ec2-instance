# Locals

locals {
  kms_key_id                        = "${element(concat(list(var.kms_key_id), list("")), 0)}"
  root_block_device_ebs_iops        = "${(var.root_block_device_ebs_volume_type == "io1") && (var.root_block_device_ebs_iops != 0) ? var.root_block_device_ebs_iops : 0}"
  root_block_device_ebs_volume_size = "${var.root_block_device_ebs_volume_size != "" ? var.root_block_device_ebs_volume_size : lookup(var.root_block_device_ebs_volume_size_defaults, var.instance_os, 8)}"
  data_block_device_ebs_iops        = "${(var.data_block_device_ebs_volume_type == "io1") && (var.data_block_device_ebs_iops != 0) ? var.data_block_device_ebs_iops : 0}"
  data_block_device_ebs_snapshot_id = "${!var.enable_encryption ? var.data_block_device_ebs_snapshot_id : element(list(""), 0)}"
  data_block_device_ebs_name_prefix = "${list("f", "g", "h", "i", "j", "k")}"
  instance_ids                      = "${concat(aws_instance.main.*.id, list(""))}"
  instance_ips                      = "${concat(aws_instance.main.*.private_ip, list(""))}"
  image_id                          = "${var.image_id != "" ? var.image_id : lookup(map("linux", data.aws_ami.linux.id, "windows", data.aws_ami.windows.id), var.instance_os, data.aws_ami.linux.id)}"
  instance_name_prefix              = "${lower(var.instance_name_prefix != "" ? var.instance_name_prefix : var.name)}"
  key_name                          = "${element(concat(aws_key_pair.main.*.key_name, list(var.key_name), list("")), 0)}"
  template_cloudinit_config_main    = "${concat(data.template_cloudinit_config.main.*.rendered, list(""))}"
  template_cloudinit_config_custom  = "${base64encode(element(concat(data.template_file.custom_userdata.*.rendered, list(var.custom_userdata_script_content), list("")), 0))}"
  target_group_arns                 = "${element(concat(var.target_group_arns, list("")), 0)}"
}

# Resource Elastic IP

resource "aws_eip" "main" {
  count = "${var.create_instance && var.create_eip ? var.instance_count : 0 }"

  vpc      = true
  instance = "${element(local.instance_ids, count.index)}"

  tags = "${merge(map("Name", format("%s%d-%s", local.instance_name_prefix, (count.index + 1), var.resource_identities["eip"],)), var.global_tags, var.module_tags, var.eip_tags)}"
}

# Resource Key Pair

resource "aws_key_pair" "main" {
  count = "${var.create_instance && var.create_key_pair ? 1 : 0 }"

  key_name   = "${format("%s-%s", var.name, var.resource_identities["key_name"],)}"
  public_key = "${var.public_key}"
}

# Resource EC2 instance UserData input

resource "random_integer" "subnet_count" {
  count = "${var.create_instance ? var.instance_count : 0 }"

  min = 0
  max = "${length(var.subnet_id) == "1" ? 1 : (length(var.subnet_id) - 1) }"
}

data "template_file" "main_userdata" {
  count = "${var.create_instance ? var.instance_count : 0 }"

  template = "${file("${path.module}/templates/${var.instance_os}_init.tpl")}"

  vars {
    hostname = "${format("%s%d", local.instance_name_prefix, (count.index + 1))}"
  }
}

data "template_cloudinit_config" "main" {
  count = "${var.create_instance ? var.instance_count : 0 }"

  gzip          = "${var.instance_os == "windows" ? false : true }"
  base64_encode = true

  part {
    filename     = "sethostname.script"
    content_type = "text/x-shellscript"
    content      = "${element(data.template_file.main_userdata.*.rendered, count.index)}"
  }
}

data "template_file" "custom_userdata" {
  count = "${var.create_instance && var.create_custom_userdata && var.custom_userdata_script_path != "" ? 1 : 0 }"

  template = "${file("${var.custom_userdata_script_path}")}"
}

# EC2 user Data Volume disks

resource "aws_ebs_volume" "main" {
  count = "${var.create_instance && var.create_data_block_device_ebs ? (var.instance_count * var.data_block_device_ebs_count) : 0 }"

  availability_zone = "${element(var.subnet_az, element(random_integer.subnet_count.*.result, ceil(count.index / var.data_block_device_ebs_count)))}"
  encrypted         = "${var.enable_encryption}"
  iops              = "${local.data_block_device_ebs_iops}"
  size              = "${var.data_block_device_ebs_volume_size}"
  snapshot_id       = "${local.data_block_device_ebs_snapshot_id}"
  type              = "${var.data_block_device_ebs_volume_type}"
  kms_key_id        = "${local.kms_key_id}"

  tags = "${merge(map("Name", format("%s%d-data-/dev/xvd%s", local.instance_name_prefix, (ceil(count.index / var.data_block_device_ebs_count) + 1) , element(local.data_block_device_ebs_name_prefix, ceil(count.index / var.instance_count)))), var.global_tags, var.module_tags, var.data_block_device_tags)}"
}

resource "aws_volume_attachment" "main" {
  count = "${var.create_instance && var.create_data_block_device_ebs ? var.instance_count * var.data_block_device_ebs_count : 0 }"

  device_name  = "/dev/xvd${element(local.data_block_device_ebs_name_prefix, ceil(count.index / var.instance_count))}"
  volume_id    = "${element(aws_ebs_volume.main.*.id, count.index)}"
  instance_id  = "${element(aws_instance.main.*.id, ceil(count.index / var.data_block_device_ebs_count))}"
  force_detach = "${var.force_detach}"
  skip_destroy = "${var.skip_destroy}"
}

# EC2 Instance

resource "aws_instance" "main" {
  count = "${var.create_instance ? var.instance_count : 0 }"

  ami                     = "${local.image_id}"
  ebs_optimized           = "${var.ebs_optimized}"
  disable_api_termination = "${var.disable_api_termination}"
  instance_type           = "${var.instance_type}"
  key_name                = "${local.key_name}"
  get_password_data       = "${var.instance_os == "windows" && local.key_name != "" ? true : false }"
  vpc_security_group_ids  = ["${var.vpc_security_group_ids}"]
  subnet_id               = "${element(var.subnet_id, element(random_integer.subnet_count.*.result, count.index))}"
  user_data_base64        = "${var.create_custom_userdata ? local.template_cloudinit_config_custom : element(local.template_cloudinit_config_main, count.index)}"
  iam_instance_profile    = "${var.iam_instance_profile}"

  root_block_device {
    volume_type           = "${var.root_block_device_ebs_volume_type}"
    volume_size           = "${local.root_block_device_ebs_volume_size}"
    iops                  = "${local.root_block_device_ebs_iops}"
    delete_on_termination = "${var.root_block_device_ebs_delete_on_termination}"
  }

  lifecycle {
    ignore_changes = ["private_ip", "ami", "root_block_device", "ebs_block_device"]
  }

  tags = "${merge(map("Name", format("%s", local.instance_name_prefix)), var.global_tags, var.module_tags, var.instance_tags)}"
}

# Target Group Association

resource "aws_lb_target_group_attachment" "main" {
  count = "${var.create_instance && var.associate_target_group_arns && !var.associate_target_group_instance_ip ? var.instance_count : 0 }"

  target_group_arn = "${local.target_group_arns}"
  target_id        = "${element(local.instance_ids, count.index)}"
}

resource "aws_lb_target_group_attachment" "ip" {
  count = "${var.create_instance && var.associate_target_group_arns && var.associate_target_group_instance_ip ? var.instance_count : 0 }"

  target_group_arn = "${local.target_group_arns}"
  target_id        = "${element(local.instance_ips, count.index)}"
}
